﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportExport.Models
{
    public class Students
    {
        public string StudentID { get; set; }

        public string StudentName { get; set; }

        public string DateOfBirth { get; set; }

        public string  MarkPercentage { get; set; } 

        public List<Students> StudentList { get; set; }

        //public Students()
        //{
        //    StudentList = new List<Students>();
        //}

        
    }
    public class ExcelFile
    {
        public IFormFile excelFile { get; set; }
    }
}
