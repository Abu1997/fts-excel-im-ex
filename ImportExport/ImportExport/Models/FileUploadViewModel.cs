﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportExport.Models
{
    public class FileUploadViewModel
    {
        public IFormFile XlsFile { get; set; }
        /*create StaffInfoViewModel  object because we need to add read
         excel data and mapping in StaffInfoViewModel*/
        public Students Students { get; set; }
        public FileUploadViewModel()//Create contractor
        {
            //call StaffInfoViewModel  this object in contractor
            Students = new Students();
        }
    }
}
