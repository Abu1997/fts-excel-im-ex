﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using OfficeOpenXml;
using System.Linq;
using ImportExport.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using static ImportExport.Models.Students;
using System.Collections.Generic;

namespace ExcelFileRead.Controllers
{
    [Route("api/excel")]
    [ApiController]
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        private readonly IConfiguration _configuration;
        public HomeController(IWebHostEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;

            _configuration = configuration;
        }

        [HttpPost("import")]
        public IActionResult importToDb([FromForm] ExcelFile excelFile)
        {
            var list = new List<Students>();
            var stream = new MemoryStream();
            excelFile.excelFile.CopyTo(stream);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(stream);
            var ExcelSheet = package.Workbook.Worksheets[0];
            int rowCount = ExcelSheet.Dimension.Rows;
            for (int i = 2; i < rowCount; i++)
            {
                list.Add(new Students
                {
                    StudentID = ExcelSheet.Cells[i, 1].Value.ToString().Trim(),
                    StudentName = ExcelSheet.Cells[i, 2].Value.ToString().Trim(),
                    DateOfBirth = ExcelSheet.Cells[i, 3].Value.ToString().Trim(),
                    MarkPercentage = ExcelSheet.Cells[i, 4].Value.ToString().Trim(),
                }); ;
            }

            foreach (Students item in list)
            {
                string query = "insert into Student values('" + item.StudentName + "','" + item.DateOfBirth + "','" + item.MarkPercentage + "')";

                string serverPath = _configuration.GetConnectionString("DefaultString");

                SqlDataReader reader;

                DataTable table = new DataTable();

                using (SqlConnection connection = new SqlConnection(serverPath))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        reader = command.ExecuteReader();
                        table.Load(reader);
                        connection.Close();
                        reader.Close();
                    }
                }
            }
            return Ok("updated successfully");
        }
    }
}